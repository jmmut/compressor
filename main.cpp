#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <list>
#include <vector>
#include <stack>
#include <queue>
#include <cstddef>
//#include <concepts>

template<typename T1, typename T2>
std::ostream &operator<<(std::ostream &out, const std::pair<T1, T2> &p);

template<typename T>
std::ostream &operator<<(std::ostream &out, const std::vector<T> &container);

template<typename K, typename V>
std::ostream &operator<<(std::ostream &out, const std::map<K, V> &container);

#define ASSERT_EQUALS_OR_THROW(actual, expected) ASSERT_EQUALS_OR_THROW_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_MSG(actual, expected, message) \
    if (const auto &_actualResult(actual); true)              \
        if (decltype(_actualResult) &_expectedResult(expected); \
                not ((_actualResult) == (_expectedResult)))   \
            if (std::stringstream comparisonMessage; true)    \
                throw std::runtime_error(                     \
                    ((comparisonMessage << __FILE__ << ":" << __LINE__\
                        << (": assertion (" #actual " == " #expected ") failed:\n  actual:   ") \
                        << _actualResult << "\n  expected: " << _expectedResult                 \
                        << (std::string{message}.empty()?     \
                        std::string{""} : std::string{"\n  explanation: "} + (message))         \
                        << "\n"), comparisonMessage.str()))



/**
 * A container is anything that has defined `.size()`, `.begin()` and `.end()` and can be iterated with them.
 * To print the inner types, the operator<< has to be defined for them.
 */
template<typename C>
std::string to_string(const C &container, std::string open, std::string separator, std::string close) {
    if (container.size() == 0) {
        return open + close;
    } else {
        std::stringstream ss;
        ss << open;
        auto it = container.begin();
        ss << *it;
        ++it;
        for (; it != container.end(); ++it) {
            ss << separator << *it;
        }
        ss << close;
        return ss.str();
    }
}

template<typename C>
std::string to_string(const C &container) {
    return to_string(container, "[", ", ", "]");
}


template<typename T1, typename T2>
std::ostream &operator<<(std::ostream &out, const std::pair<T1, T2> &p) {
    return out << "{" << p.first << ", " << p.second << "}";
}

template<typename T>
std::ostream &operator<<(std::ostream &out, const std::vector<T> &container) {
    return out << to_string(container);
}

template<typename K, typename V>
std::ostream &operator<<(std::ostream &out, const std::map<K, V> &container) {
    return out << to_string(container);
}

using Bits = std::vector<bool>;
using CharCounts = std::map<char, long>;
using CharToHuffmanBits = std::map<char, Bits>;
struct BNode {
    char letter;
    long occurrences;
    std::unique_ptr<BNode> left;
    std::unique_ptr<BNode> right;
    static std::unique_ptr<BNode> make_leaf(char letter, long occurrences) {
        return std::make_unique<BNode>(BNode{letter, occurrences, nullptr, nullptr});
    }
    static std::unique_ptr<BNode> make_node(std::unique_ptr<BNode> left, std::unique_ptr<BNode> right) {
        return std::make_unique<BNode>(
                BNode{ static_cast<char>(0),
                        left->occurrences + right->occurrences,
                        std::move(left),
                        std::move(right) });
    }
    bool isLeaf() const { return left == nullptr and right == nullptr; }
};


std::ostream &print(std::ostream &out, const BNode *node, int nesting = 1) {
    if (node != nullptr) {
        out << "{";
        if (node->isLeaf()) {
            return out << node->occurrences << ", " << node->letter << "}";
        } else {
            auto indent = std::string(nesting*2, ' ');
            out << node->occurrences << ",\n" << indent;
            print(out, node->left.get(), nesting + 1) << ",\n" << indent;
            print(out, node->right.get(), nesting + 1) << "}";
            return out;
        }
    } else {
        return out << "()";
    }
}

std::ostream &operator<<(std::ostream &out, const BNode *node) {
    return print(out, node);
}

std::unique_ptr<BNode> createEncodingTree(const CharCounts &charCounts) {
    auto subtrees = std::list<std::unique_ptr<BNode>>{};
    for (const auto &[letter, count] : charCounts) {
        subtrees.emplace_back(BNode::make_leaf(letter, count));
    }
    subtrees.sort([](const auto &left, const auto &right) {
        return left->occurrences < right->occurrences;
    });
    while (subtrees.size() > 1) {
        auto first = std::move(subtrees.front());
        subtrees.pop_front();
        auto second = std::move(subtrees.front());
        subtrees.pop_front();
        auto grouping = BNode::make_node(std::move(first), std::move(second));
        subtrees.push_front(std::move(grouping));
        subtrees.sort([](const auto &left, const auto &right) {
            return left->occurrences < right->occurrences;
        });
    }
    return std::move(subtrees.front());
}

// just for fun I did this iteratively but I wasted a lot of time debugging. I don't think it's worth it.
CharToHuffmanBits collapseEncodingTree(std::unique_ptr<BNode> &huffmanTree) {
    std::stack<BNode*> stack;
    stack.push(huffmanTree.get());
    std::stack<std::vector<bool>> bitsStacks;
    bitsStacks.emplace();

    auto result = CharToHuffmanBits{};
    while (not stack.empty()) {
        auto cursor = stack.top();
        stack.pop();
        
        if (cursor->right != nullptr) {
            stack.push(cursor->right.get());
        }

        if (cursor->left != nullptr) {
            stack.push(cursor->left.get());
        }

        if (cursor->isLeaf()) {
            result[cursor->letter] = bitsStacks.top();
            bitsStacks.pop();
        } else {
            bitsStacks.top().push_back(true);

            auto back = bitsStacks.top();
            back.pop_back();
            back.push_back(false);
            bitsStacks.push(back);
        }
    }
    return result;
}

CharToHuffmanBits computeHuffman(const CharCounts &charCounts) {
    auto subtree = createEncodingTree(charCounts);
//    std::cout << "tree:\n" << subtree.get() << std::endl;
    return collapseEncodingTree(subtree);
}
std::ifstream open_for_reading(std::string filename) {
    std::ifstream file{ filename, std::ios::binary };
    if (not file) {
        std::cout << "error: can't open file: " << filename << std::endl;
    }
    return file;
}

std::pair<CharCounts, long> countCharsInFile(std::string filename) {
    auto file = open_for_reading(filename);

    auto charCounts = CharCounts{};
    auto totalCount = long{};
    char c;
    while (file.read(&c, 1)) {
        totalCount++;
        charCounts[c]++;
    }
    return { charCounts, totalCount };
}

std::pair<long, long> countBits(std::string filename, const CharToHuffmanBits &huffman) {
    auto file = open_for_reading(filename);
    auto originalCount = long{};
    auto compressedCount = long{};
    char c;
    while (file.read(&c, 1)) {
        originalCount += 8;
        compressedCount += huffman.at(c).size();
    }
    return {compressedCount, originalCount};
}


double measureEntropy(const CharCounts &charCounts, long totalCount) {
    double entropy = 0;
    for (const auto &[letter, count] : charCounts) {
        auto probability = static_cast<double>(count) / totalCount;
        entropy -= probability * log2(probability);
    }
    return entropy;
}

template<typename T>
class Serializer {
public:
    explicit Serializer(const T &data) : data(data) {}

    std::ostream &writeTo(std::ostream &out) {
        return out.write(reinterpret_cast<const char *>(&data), sizeof(data));
    }
private:
    const T &data;
};

template<typename T>
class Deserializer {
public:
    explicit Deserializer(T &data) : data(data) {}

    std::istream &readFrom(std::istream &in) {
        return in.read(reinterpret_cast<char *>(&data), sizeof(data));
    }
private:
    T &data;
};

// TODO: get a compiler that supports this
//template<typename T>
//concept IsMap = requires (T map) {
//    map.begin().operator*().first;
//    map.begin().operator*().second;
//};

template<typename KEY, typename VALUE>
class Serializer<std::map<KEY, VALUE>> {
    using T = std::map<KEY, VALUE>;
public:
    explicit Serializer(const T &data) : data(data) {}

    std::ostream &writeTo(std::ostream &out) {
        auto size = data.size();
        Serializer<decltype(size)>{size}.writeTo(out);
        for (const auto &[key, value] : data) {
            Serializer<KEY>{key}.writeTo(out);
            Serializer<VALUE>{value}.writeTo(out);
        }
        return out;
    }
private:
    const T &data;
};

template<typename KEY, typename VALUE>
class Deserializer<std::map<KEY, VALUE>> {
    using T = std::map<KEY, VALUE>;
public:
    explicit Deserializer(T &data) : data(data) {}

    std::istream &readFrom(std::istream &in) {
        auto size = decltype(data.size()){};
        Deserializer<decltype(size)>{size}.readFrom(in);
        for (size_t i = 0; i < size; ++i) {
            KEY key;
            VALUE value;
            Deserializer<decltype(key)>{key}.readFrom(in);
            Deserializer<decltype(value)>{value}.readFrom(in);
            data[key] = value;
        }
        return in;
    }
private:
    T &data;
};
//template<>
//class Deserializer<std::map<char, Bits>> {
//    using T = std::map<char, Bits>;
//public:
//    explicit Deserializer(T &data) : data(data) {}
//
//    std::istream &readFrom(std::istream &in) {
//        auto size = decltype(data.size()){};
//        Deserializer<decltype(size)>{size}.readFrom(in);
//        for (size_t i = 0; i < size; ++i) {
//            typename T::key_type key;
//            typename T::mapped_type value;
//            Deserializer<decltype(key)>{key}.readFrom(in);
//            Deserializer<decltype(value)>{value}.readFrom(in);
//            data[key] = value;
//        }
//        return in;
//    }
//private:
//    T &data;
//};

std::ofstream open_for_writing(std::string filename) {
    auto file = std::ofstream{ filename };
    if (not file) {
        std::cout << "error: can't open file: " << filename << std::endl;
    }
    return file;
}
class BufferedBitsWriter {
public:
    BufferedBitsWriter(std::ostream &out) : buffer{}, out(out) {}


    // dumbest implementation ever. Wasted too much time trying to do something reasonable
    BufferedBitsWriter &operator<<(const Bits &bits) {
        for (auto bit : bits) {
            buffer.push(bit);
        }
        while (buffer.size() > chunkSizeInBits) {
            writeChunkOfBitsOrLess(chunkSizeInBits);
        }
        return *this;
    }

    virtual ~BufferedBitsWriter() {
        if (buffer.size() > 0) {
            writeChunkOfBitsOrLess(buffer.size());
        }
    }

private:
    void writeChunkOfBitsOrLess(size_t bitsCount) {
        if (bitsCount > chunkSizeInBits) {
            throw std::runtime_error{
                    "logic error: writeChunkOfBitsOrLess should be called to write a single chunk of up to "
                            + std::to_string(chunkSizeInBits) + " bits, but was requested to write "
                            + std::to_string(bitsCount) + " bits" };
        }
        auto chunk = ChunkType{0};
        for (int i = 0; i < bitsCount; ++i) {
            auto bit = buffer.front();
            buffer.pop();
            auto b = ChunkType{bit};
            chunk |= (b << i);
        }
//        std::cout << "writing " << bitsCount << " bits: " << std::hex << static_cast<int>(chunk) << std::endl;
        out.write(reinterpret_cast<char*>(&chunk), sizeof(ChunkType));
    }

    using ChunkType = uint8_t;
    int chunkSizeInBits = sizeof(ChunkType) * 8;
    std::queue<bool> buffer;
    std::ostream &out;
};

class BitIstream {
public:
    BitIstream(std::istream &inputStream) : inputStream(inputStream) {}

    bool advance() {
        if (buffer.empty()) {
            uint8_t chunk;
            inputStream.read(reinterpret_cast<char*>(&chunk), sizeof(chunk));
//            std::cout << "\nread chunk " << std::hex << static_cast<int>(chunk) << "   ";
            if (not inputStream) {
                return false;
            }
            for (int i = 0; i < sizeof(chunk) * 8; ++i) {
                buffer.push(chunk & (1ul << i));
            }
        }

        current = buffer.front();
        buffer.pop();
        return true;
    }

    char get() {
        return current;
    }
private:
    std::istream &inputStream;
    char current;
    std::queue<bool> buffer;
};

template<>
class Serializer<Bits> {
    using T = Bits;
public:
    explicit Serializer(const T &data) : data(data) {}

    std::ostream &writeTo(std::ostream &out) {
        auto size = data.size();
        Serializer<decltype(size)>{size}.writeTo(out);
        auto writer = BufferedBitsWriter{out};
        writer << data;
        return out;
    }
private:
    const T &data;
};

template<>
class Deserializer<Bits> {
    using T = Bits;
public:
    explicit Deserializer(T &data) : data(data) {}

    std::istream &readFrom(std::istream &in) {
        auto size = decltype(data.size()){};
        Deserializer<decltype(size)>{size}.readFrom(in);
        auto reader = BitIstream{in};
        for (int i = 0; i < size and reader.advance(); ++i) {
            data.push_back(reader.get());
        }
        return in;
    }
private:
    T &data;
};

void writeCompressed(std::string filename, const CharToHuffmanBits &huffman, int64_t letterCount) {
    auto compressedFile = open_for_writing(filename + ".compressed");
    Serializer{letterCount}.writeTo(compressedFile);
    Serializer{huffman}.writeTo(compressedFile);

    auto file = open_for_reading(filename);
    char c;
    auto bufferedBitsWriter = BufferedBitsWriter{compressedFile};
    while(file.read(&c, sizeof(c))) {
        bufferedBitsWriter << huffman.at(c);
    }
}

void compress(std::string filename) {
    std::cout << "counting chars..." << std::endl;
    auto [charCounts, totalCount] = countCharsInFile(filename);
//    std::cout << "total chars count: " << totalCount << ", counts per char: " << to_string(charCounts, "[\n", ",\n", "]") << std::endl;

    auto huffman = computeHuffman(charCounts);
    std::cout << "huffman encoding: " << to_string(huffman, "[\n", ",\n", "]") << std::endl;

    std::cout << "counting bits..." << std::endl;
    std::cout << "compressed and original bit count: " << countBits(filename, huffman) << std::endl;
    auto entropy = measureEntropy(charCounts, totalCount);
    std::cout << "entropy: " << entropy << " bits/letter, " << entropy * totalCount << " bits in total" << std::endl;

    std::cout << "writing compressed file..." << std::endl;
    writeCompressed(filename, huffman, totalCount);
}

//template<typename T>
//concept IsBoolIstream = requires (T t, bool b) {t >> b;};

template<typename T>
class HuffmanDecoder {
public:
    HuffmanDecoder(const BNode &huffmanTree, T &in, long expectedReadCount)
    : huffmanTree{huffmanTree}, inputStream{in}, current{0}, readCount{0}, expectedReadCount{expectedReadCount} {
        // TODO: handle no-entropy: "aaaaa"
        if (huffmanTree.isLeaf()) {
            throw std::runtime_error{ "unimplemented: decoding with just 1 code word" };
        }
    }

    bool advance() {
        if (readCount == expectedReadCount) {
            return false;
        }
        auto currentNode = &huffmanTree;
        do {
            if (not inputStream.advance()) {
                return false;
            }
            auto bit = inputStream.get();
            if (bit) {
                currentNode = currentNode->right.get();
            } else {
                currentNode = currentNode->left.get();
            }
        } while (not currentNode->isLeaf());

        current = currentNode->letter;
        readCount++;
        return true;
    }

    char get() {
        return current;
    }

    virtual ~HuffmanDecoder() {
        if (readCount != expectedReadCount) {
            if (inputStream.advance()) {
                std::cout << "warn: Huffman decoder being destructed when the inner stream still has data";
            }
        }
    }

private:
    const BNode &huffmanTree;
    T &inputStream;
    char current;
    long readCount;
    long expectedReadCount;
};


std::unique_ptr<BNode> buildTreeFromEncodingTable(CharToHuffmanBits huffmanTable) {
    auto root = BNode::make_leaf(0, 0);
    auto createIfNull = [](auto &node) -> decltype(node)& {
        if (node == nullptr) {
            node = BNode::make_leaf(0, 0);
        }
        return node;
    };
    for (const auto &[letter, bits] : huffmanTable) {
        auto *cursor = root.get();
        for(auto bit : bits) {
            cursor = createIfNull(bit? cursor->right : cursor->left).get();
        }
        cursor->letter = letter;
    }
    return root;
}

void decompress(std::string filename) {

    std::cout << "preparing decompression..." << std::endl;
    auto file = open_for_reading(filename);
    int64_t decompressedSize;
    Deserializer{decompressedSize}.readFrom(file);
    auto huffmanTable = CharToHuffmanBits{};
    Deserializer{huffmanTable}.readFrom(file);
    auto huffmanTree = buildTreeFromEncodingTable(huffmanTable);
    auto bitStream = BitIstream{file};

    std::cout << "decoding file..." << std::endl;
    auto decoder = HuffmanDecoder{huffmanTree.operator*(), bitStream, decompressedSize};
    auto decompressedFile = open_for_writing(filename + ".decompressed");
    while (decoder.advance()) {
        decompressedFile << decoder.get();
    }
}

void debugDecompression(std::string filename) {
    auto file = open_for_reading(filename);
    int64_t decompressedSize;
    Deserializer{decompressedSize}.readFrom(file);
    auto huffmanTable = CharToHuffmanBits{};
    Deserializer{huffmanTable}.readFrom(file);
    std::cout << "loaded huffman encoding: " << to_string(huffmanTable, "[\n", ",\n", "]") << std::endl;
    auto huffmanTree = buildTreeFromEncodingTable(huffmanTable);
    auto bitStream = BitIstream{file};
    std::cout << "bits: ";
    while (bitStream.advance()) {
        std::cout << bitStream.get();
    }
    std::cout << std::endl;
}

void test() {
    std::cout << "before test" << std::endl;

    {
        auto subtrees = std::list<std::unique_ptr<BNode>>{};
        subtrees.emplace_back(
                BNode::make_node(
                        BNode::make_node(
                                BNode::make_leaf('a', 4),
                                BNode::make_node(
                                        BNode::make_leaf('c', 2),
                                        BNode::make_leaf('b', 3))),
                        BNode::make_leaf('d', 10)));
        auto huffman = collapseEncodingTree(subtrees.back());
        ASSERT_EQUALS_OR_THROW(huffman['d'], (std::vector<bool>{ true }));
        ASSERT_EQUALS_OR_THROW(huffman['a'], (std::vector<bool>{ false, false }));
        ASSERT_EQUALS_OR_THROW(huffman['b'], (std::vector<bool>{ false, true, true }));
        ASSERT_EQUALS_OR_THROW(huffman['c'], (std::vector<bool>{ false, true, false }));
    }

    {
        auto charCounts = CharCounts{{ 'a', 4 },
                                     { 'b', 3 },
                                     { 'c', 2 }};
        auto huffman = computeHuffman(charCounts);
        ASSERT_EQUALS_OR_THROW(huffman.size(), 3);
        ASSERT_EQUALS_OR_THROW(huffman.at('a').size(), 1);
        ASSERT_EQUALS_OR_THROW(huffman.at('b').size(), 2);
        ASSERT_EQUALS_OR_THROW(huffman.at('c').size(), 2);
    }

    {
        auto charCounts = CharCounts{{ 'a', 4 },
                                     { 'b', 3 },
                                     { 'c', 2 }};
        auto entropy = measureEntropy(charCounts, 9);
        ASSERT_EQUALS_OR_THROW_MSG(entropy > 1.5, true, std::to_string(entropy));
        ASSERT_EQUALS_OR_THROW_MSG(entropy < 1.6, true, std::to_string(entropy));
    }

    {
        auto filename = "test.bits";
        {
            auto bits = Bits{ true };
            auto file = open_for_writing(filename);
            {
                auto buf = BufferedBitsWriter{ file };
                buf << bits;
            }
        }

        auto file = open_for_reading(filename);
        uint64_t n;
        file >> n;
        std::filesystem::remove(filename);
//        ASSERT_EQUALS_OR_THROW(n, 1);
    }
    {
        auto charCounts = CharCounts{{ 'a', 4 },
                                     { 'b', 3 },
                                     { 'c', 2 }};
        auto tree = createEncodingTree(charCounts);
        auto huffmanTable = computeHuffman(charCounts);
        auto ss = std::stringstream{};
        {
            auto buf = BufferedBitsWriter{ ss };
            buf << huffmanTable['a'] << huffmanTable['b'] << huffmanTable['c'];
            buf << huffmanTable['a'] << huffmanTable['b'] << huffmanTable['c'];
        }
        ss.seekg(0);
//        char c;
//        ss >> c;
//        ASSERT_EQUALS_OR_THROW(c, 0b0101'1010);
        auto bitStream = BitIstream{ss};
        auto decoder = HuffmanDecoder{tree.operator*(), bitStream, 6};
        auto result = std::string{};
        while (decoder.advance()) {
            result += decoder.get();
        }
        ASSERT_EQUALS_OR_THROW(result, "abcabc");
    }
    {
        auto testMap = std::map<char, int>{{'a', 3}, {'b', 5}};
        auto filename = "test.compressed";
        {
            auto file = open_for_writing(filename);
            Serializer{ testMap }.writeTo(file);
        }
        auto readMap = decltype(testMap){};
        auto in = open_for_reading(filename);
        Deserializer{readMap}.readFrom(in);
        std::filesystem::remove(filename);
        ASSERT_EQUALS_OR_THROW(readMap, testMap);
    }
    std::cout << "past test" << std::endl;
}


void printUsage(char *const *argv) {
    std::cout << "usage:\n  "
          << argv[0] << " <file_to_compress>\n  "
          << argv[0] << " -d <file_to_decompress>" << std::endl;
}

bool isTestArgs(int argc, char **argv) {
    return argc == 2 and argv[1] == std::string{"test"};
}
bool isCompressionArgs(int argc, char **argv) {
    return argc == 2 and std::filesystem::is_regular_file(argv[1]);
}
bool isDecompressionArgs(int argc, char **argv) {
    return argc == 3 and argv[1] == std::string{"-d"} and std::filesystem::is_regular_file(argv[2]);
}
bool isDebugArgs(int argc, char **argv) {
    return argc == 3 and argv[1] == std::string{"debug"} and std::filesystem::is_regular_file(argv[2]);
}

int main(int argc, char **argv) {
    if (isTestArgs(argc, argv)) {
        test();
    } else if (isCompressionArgs(argc, argv)) {
        compress(argv[1]);
    } else if (isDecompressionArgs(argc, argv)) {
        decompress(argv[2]);
    } else if (isDebugArgs(argc, argv)) {
        debugDecompression(argv[2]);
    } else {
        printUsage(argv);
        return 1;
    }
    return 0;
}
