
set -euo pipefail

if [ $# != 1 ]
then
  if [$# != 2]
  then
    echo "usage: $0 file_to_compress"
    exit 1
  fi
fi

file_to_compress=$1

build_folder=cmake-build-relwithdebinfo
mkdir -p $build_folder && cd $build_folder

cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
make

cd ..

$build_folder/compressor $1
$build_folder/compressor -d $1.compressed

echo ""
echo "searching for differences between the original and the decompressed files..."
set +e
diff $1 $1.compressed.decompressed
set -e
echo "comparison done"
md5sum $1 $1.compressed.decompressed
if [ $# == 2 ]
then
  $build_folder/compressor debug $1.compressed
fi

